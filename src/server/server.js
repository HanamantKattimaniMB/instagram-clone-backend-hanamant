const express = require('express')
const envVariable = require('./config')
const routeroObj = require('./Router')

const app = express()

app.use('/api',routeroObj)

app.use((req,res,next) => { 
    res.status(404)
    res.json({Error:'Page not found'})
    res.end()
    next()
})

app.listen(envVariable.PORT)
console.log(`Server is started on ${envVariable.PORT}`)