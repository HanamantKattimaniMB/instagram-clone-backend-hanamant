const path=require('path')
const mysql = require('mysql');
const envVariable = require('./config');
const { getMaxListeners } = require('process');
const { log } = require('console');

var connection = mysql.createPool({
  host     : envVariable.DB_HOST,
  user     : envVariable.DB_USER,
  password : envVariable.DB_PASS,
  database : envVariable.DB_NAME,
})

connection.getConnection((err,connection) => {
  if(err){
    console.log("Error occurred",err)
  }else{
    connection.release()
  }
})

let QueryToCreateSignupTable = `CREATE TABLE IF NOT EXISTS Signup(user_id INT PRIMARY KEY AUTO_INCREMENT,username VARCHAR(100), email VARCHAR(100), phone VARCHAR(20), fullName VARCHAR(100), password VARCHAR(100))`
function creatTable(QueryToCreateTable) {
  connection.query(QueryToCreateTable,(error,data,field) => {
    if(error) {
      throw error;
    } 
  })
}
creatTable(QueryToCreateSignupTable)

function getSignupData(director_name){
  return new Promise((resolve,reject) => {
    connection.query(`SELECT * FROM Signup`, (error, data, field) => {
      if(error){
        reject(error)
      } 
      resolve(data)
    })  
  })
}

function insertDataInSignup(director_name){
  return new Promise((resolve,reject) => {
    connection.query(`INSERT INTO Signup (username, email, phone, fullName, password) values (?)`,[director_name],(error,data,field) => {
      if(error){
        reject(error)
      } 
      resolve(data)
    })  
  })
}

function getUsernameAndPassword(tableName, userName) {
    return new Promise((resolve,reject) => {
      connection.query(`select username,password from ${tableName} where username=?`,userName,(error,data,field) => {
        if(error){
          reject(error)
        } 
        resolve(data)
      })  
    })
}

module.exports = {
  insertDataInSignup,
  getUsernameAndPassword,
  getSignupData,
}




  

