const express = require('express')
const router = express.Router()
const modules=require('./signUpData.js')

router.use(express.json())
router.use(express.urlencoded({extended:false}))

router.get('/users/:id', function (req, res) {
    let receivedUsername = req.params.id
    modules.getUsernameAndPassword('Signup', receivedUsername)
    .then((data) => {
        res.json(data)
    })
    .catch((error)=>{
        console.log(error)
    })
})

router.post('/signup',(req, res, next) => {
    res.send(req.body)
    signupDataOfCurrentUser=Object.values(req.body)
    console.log(signupDataOfCurrentUser)
    modules.insertDataInSignup(signupDataOfCurrentUser)
})

module.exports = router