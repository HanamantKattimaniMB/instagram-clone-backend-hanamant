const dotenv=require('dotenv')
const path=require('path')
dotenv.config({
    path:path.join(__dirname,'../../.env')
})

console.log(process.env.DB_NAME)
module.exports={
   PORT:process.env.PORT,
   DB_HOST:process.env.DB_HOST,
   DB_USER:process.env.DB_USER,
   DB_PASS:process.env.DB_PASS,
   DB_NAME:process.env.DB_NAME,
}